import turtle, time

wn = turtle.Screen()
wn.bgcolor('silver')

turtle.write('I love pizza', move=False, align="center", font=("Arial", 10, "normal"))
time.sleep(1)
turtle.penup()
turtle.right(90)
turtle.forward(14)
turtle.pendown()

turtle.write('pizza ' * 20, move=False, align="center", font=("Arial", 10, "normal"))
turtle.penup()
turtle.forward(14)
turtle.pendown()
time.sleep(1)

turtle.write('cat ' + 'dog', move=False, align="center", font=("Arial", 10, "normal"))
turtle.penup()
turtle.forward(14)
turtle.pendown()
time.sleep(1)

turtle.write('Ням-ням', move=False, align="center", font=("Arial", 10, "normal"))
turtle.penup()
turtle.forward(14)
turtle.pendown()

wn.exitonclick()