import turtle, time

wn = turtle.Screen()
wn.bgcolor('silver')

liza = turtle.Turtle()
liza.color('red')
liza.pensize(10)
liza.shape('turtle')

wn = turtle.Screen()
wn.bgcolor('silver')

liza.forward(100)
time.sleep(0.5)
liza.left(90)
time.sleep(0.5)
liza.penup()
liza.forward(100)
time.sleep(0.5)
liza.right(90)
time.sleep(0.5)
liza.pendown()
liza.color('blue')
liza.forward(100)
time.sleep(0.5)

vova = turtle.Turtle()
vova.shape('turtle')
vova.backward(150)
time.sleep(0.5)
vova.speed(2)

turtle.penup()
turtle.right(90)
turtle.forward(30)
turtle.pendown()
turtle.write("That's all folks!", move=False, align="center", font=("Arial", 10, "normal"))
turtle.penup()
turtle.forward(14)


wn.exitonclick()
